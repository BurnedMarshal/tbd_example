export interface IUser {
  [user: string]: String;
}

export const users: IUser = {
  tiziano: 'ciao',
  marco: 'mondo'
}