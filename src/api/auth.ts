import { Router } from 'express';
import { users } from '../../resources/users';
import jwt from 'jsonwebtoken';

export const authAPI = Router();
export const AUTH_SECRET = 'mysecret';

authAPI.post('/', (req, res) => {
  const { user, password } = (req.body as { user: string, password: string });
  const userSelected = users[user];

  if (!!userSelected && userSelected === password) {
    res.end(JSON.stringify({ token: jwt.sign({ user: user }, AUTH_SECRET) }, null, 2));
  } else {
    res.status(403).send('invalid credentials');
  }
});
