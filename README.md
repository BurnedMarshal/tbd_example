# Trunk Based Development

Questo è un progetto di esercitazione per il trunk.

## Installazione

Dopo aver installato globalmente yarn con il comando `npm install -g yarn` eseguire il comando `yarn install`. 

## Build

Per eseguire il build del progetto utilizzare il comando `yarn build`

## Esecuzione

Il progetto si avvia con il comando `yarn start` e sarà in ascolto sulla porta `3000` all'url [http://localhost:3000/home.html](http://localhost:3000/home.html).

