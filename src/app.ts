import express from "express";
import bodyParser from "body-parser";
import { authAPI } from "./api/auth";

import fs from "fs";

interface Posts {
  title: string;
  description: string;
}

const app = express();

// Parse the incoming request body. This is needed by Passport spid strategy.
app.use(bodyParser.json());

// Parse an urlencoded body.
app.use(bodyParser.urlencoded({ extended: true }));

// APIs
app.use("/auth", authAPI);

app.use(express.static("public"));

app.get("/posts", (req, res) => {
  try {
    var posts: Posts = JSON.parse(fs.readFileSync("posts.json", "utf8"));
    res.status(200).json(posts);
  } catch (err) {
    res.status(404).json({ message: "posts not found" });
  }
});

app.use(function(req, res, next) {
    res.status(404).redirect('http://localhost:3000/404.html');
  });

export default app;
